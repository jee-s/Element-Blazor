# Element-Blazor
[![Nuget](https://img.shields.io/nuget/dt/Element.svg)](https://www.nuget.org/packages/Element/)

## 介绍
Element的Blazor版本

API 模仿 Element，CSS 直接使用 Element的样式，HTML 结构直接使用 Element 的 HTML 结构

Element 地址：https://element.eleme.cn/#/zh-CN/component/layout

Element-Blazor 演示地址：https://element-blazor.github.io/



## 社区讨论，相关文档
https://github.com/Element-Blazor/Element-Blazor/issues
 
## 使用前提
参考Blazor使用的前提条件

1. 安装 .Net 6.0
2. 安装 VS2022 ，更新到最新版

## 源码说明

拉取代码，用 VS2022 打开，直接启动 Element.ServerRender 项目

## 案例展示

地址：https://element-blazor.github.io/
